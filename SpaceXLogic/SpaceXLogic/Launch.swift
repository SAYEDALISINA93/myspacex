//
//  Launch.swift
//  SpaceXLogic
//
//  Created by Alisina on 20.05.23.
//

import Foundation

public struct Launch: Codable {
    public let name: String
    public let flightNumber: Int
    public let links: LaunchLinks
    public let launchDate: String
    public let status: Bool?
    
    public var formattedLaunchDate: String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if let date = dateFormatter.date(from: launchDate) {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return dateFormatter.string(from: date)
        }
        return nil
    }

    private enum CodingKeys: String, CodingKey {
        case name
        case flightNumber = "flight_number"
        case links
        case launchDate = "date_utc"
        case status = "success"
    }
    
    
}

public struct LaunchLinks: Codable {
    public let patch: LaunchPatch
    public let reddit: LaunchReddit
    public let flickr: LaunchFlickr
    public let presskit: String?
    public let webcast: String?
    public let youtubeID: String?
    public let article: String?
    public let wikipedia: String?
    
    private enum CodingKeys: String, CodingKey {
        case patch
        case reddit
        case flickr
        case presskit
        case webcast
        case youtubeID = "youtube_id"
        case article
        case wikipedia
    }
}

public struct LaunchPatch: Codable {
    public let small: String?
    public let large: String?
}

public struct LaunchReddit: Codable {
    public let campaign: String?
    public let launch: String?
    public let media: String?
    public let recovery: String?
}

public struct LaunchFlickr: Codable {
    public let small: [String]?
    public let original: [String]?
}

public struct LaunchLogo: Codable {
    public let small: String?
    public let large: String?
}
