//
//  NetworkManager.swift
//  SpaceXLogic
//
//  Created by Alisina on 20.05.23.
//

import Foundation

/// Handle all network calles
class NetworkManager {
    static let shared = NetworkManager()
    
    private init() {}
    
    /// Fetchs the data from API
    /// - Parameter completion: return the fetched data as Result type.
    func fetchLaunches(completion: @escaping (Result<[Launch], Error>)->Void) {
        
        guard let url = URL(string: "https://api.spacexdata.com/v4/launches") else {
            completion(.failure(NetworkError.invalidURL))
            return
        }
        
        DispatchQueue.main.async {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    completion(.failure(error))
                    return
                }
                
                guard let httpResponse = response as? HTTPURLResponse,
                      (200...299).contains(httpResponse.statusCode) else {
                    completion(.failure(NetworkError.invalidResponse))
                    return
                }
                
                guard let data = data else {
                    completion(.failure(NetworkError.noData))
                    return
                }
                
                do {
                    let launches = try JSONDecoder().decode([Launch].self, from: data)
                    completion(.success(launches))
                }catch {
                    completion(.failure(error))
                }
                
            }
            
            task.resume()
        }
    }
}

enum NetworkError: Error {
case invalidURL
case invalidResponse
case noData
}
