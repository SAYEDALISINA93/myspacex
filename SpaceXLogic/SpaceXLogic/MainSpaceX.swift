//
//  MainSpaceX.swift
//  SpaceXLogic
//
//  Created by Alisina on 20.05.23.
//

import Foundation

/// This class is handle all the business logic 
public class MainSpaceX{
    
    public init(){}
    
    /// Search for All Launches for the provided search data
    /// - Parameters:
    ///   - for: what user searching for
    ///   - listOfLaunches: return list of launches 
    public func searchLaunches(search for: String, listOfLaunches: @escaping([Launch]) -> Void)
    {
        DispatchQueue.main.async {
            NetworkManager.shared.fetchLaunches { result in
                switch result{
                case .success(let launches):
                    listOfLaunches(launches)
                case .failure(let err):
                    print("Error: \(err)")
                    listOfLaunches([])
                }
            }
        }
    }
}
