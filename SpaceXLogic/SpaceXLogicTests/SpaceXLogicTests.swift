//
//  SpaceXLogicTests.swift
//  SpaceXLogicTests
//
//  Created by Alisina on 20.05.23.
//

import XCTest
@testable import SpaceXLogic

final class SpaceXLogicTests: XCTestCase {

    func testSearchLaunches_Success() {
        let expectation = XCTestExpectation(description: "Fetch launches")
        
        let mainSpaceX = MainSpaceX()
        
        mainSpaceX.searchLaunches(search: "test") { launches in
            XCTAssertFalse(launches.isEmpty, "No launches found")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testSearchLaunches_Failure() {
        let expectation = XCTestExpectation(description: "Fetch launches")
        
        let mainSpaceX = MainSpaceX()
        
        mainSpaceX.searchLaunches(search: "invalid") { launches in
            XCTAssertTrue(launches.isEmpty, "Launches found for invalid search")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 10.0)
    }

    func testFetchLaunches_Success() {
        let expectation = XCTestExpectation(description: "Fetch launches")
        
        NetworkManager.shared.fetchLaunches { result in
            switch result {
            case .success(let launches):
                XCTAssertFalse(launches.isEmpty, "No launches found")
                expectation.fulfill()
            case .failure(let error):
                XCTFail("Failed to fetch launches: \(error.localizedDescription)")
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testFetchLaunches_Failure() {
        let expectation = XCTestExpectation(description: "Fetch launches")
        
        NetworkManager.shared.fetchLaunches { result in
            switch result {
            case .success(let launches):
                XCTFail("Unexpectedly received launches: \(launches.count)")
            case .failure(let error):
                XCTAssertNotNil(error, "Error should be returned")
                expectation.fulfill()
            }
        }
        
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testDecodingLaunch() throws {
        let launchJSON = """
        {
            "name": "Test Launch",
            "flight_number": 1,
            "links": {
                "patch": {
                    "small": "https://test.com/patch_small.jpg",
                    "large": "https://test.com/patch_large.jpg"
                },
                "reddit": {
                    "campaign": "https://reddit.com/campaign",
                    "launch": "https://reddit.com/launch",
                    "media": "https://reddit.com/media",
                    "recovery": "https://reddit.com/recovery"
                },
                "flickr": {
                    "small": ["https://flickr.com/photos/small1.jpg", "https://flickr.com/photos/small2.jpg"],
                    "original": ["https://flickr.com/photos/original1.jpg", "https://flickr.com/photos/original2.jpg"]
                },
                "presskit": "https://test.com/presskit.pdf",
                "webcast": "https://test.com/webcast",
                "youtube_id": "abc123",
                "article": "https://test.com/article",
                "wikipedia": "https://test.com/wikipedia"
            },
            "date_utc": "2023-01-01",
            "success": true
        }
        """.data(using: .utf8)!
        
        let decoder = JSONDecoder()
        let launch = try decoder.decode(Launch.self, from: launchJSON)
        
        XCTAssertEqual(launch.name, "Test Launch")
        XCTAssertEqual(launch.flightNumber, 1)
        XCTAssertNotNil(launch.links)
        XCTAssertEqual(launch.launchDate, "2023-01-01")
        XCTAssertEqual(launch.status, true)
    }
    
    func testDecodingLaunch_Failure() throws {
        let launchJSON = """
        {
            "name": "Test Launch",
            "flight_number": "invalid",
            "links": {
                "patch": {
                    "small": "https://test.com/patch_small.jpg",
                    "large": "https://test.com/patch_large.jpg"
                },
                "reddit": {
                    "campaign": "https://reddit.com/campaign",
                    "launch": "https://reddit.com/launch",
                    "media": "https://reddit.com/media",
                    "recovery": "https://reddit.com/recovery"
                },
                "flickr": {
                    "small": ["https://flickr.com/photos/small1.jpg", "https://flickr.com/photos/small2.jpg"],
                    "original": ["https://flickr.com/photos/original1.jpg", "https://flickr.com/photos/original2.jpg"]
                },
                "presskit": "https://test.com/presskit.pdf",
                "webcast": "https://test.com/webcast",
                "youtube_id": "abc123",
                "article": "https://test.com/article",
                "wikipedia": "https://test.com/wikipedia"
            },
            "date_utc": "2023-01-01",
            "success": true
        }
        """.data(using: .utf8)!
        
        let decoder = JSONDecoder()
        
        XCTAssertThrowsError(try decoder.decode(Launch.self, from: launchJSON)) { error in
            XCTAssertTrue(error is DecodingError, "Expected DecodingError")
        }
    }

}
