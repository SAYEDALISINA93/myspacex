//
//  MySpaceXTests.swift
//  MySpaceXTests
//
//  Created by Alisina on 20.05.23.
//

import XCTest
@testable import SpaceXLogic

final class MySpaceXTests: XCTestCase {

    var mySpaceX: MainSpaceX!
    
    override func setUpWithError() throws {
        mySpaceX = MainSpaceX()
    }

    override func tearDownWithError() throws {
        mySpaceX = nil
    }

    func testSearchLaunches_WithEmptyKeyword_ReturnsFilteredLaunches() {
        let expectation = XCTestExpectation(description: "Search launches with empty keyword")
        
        mySpaceX.searchLaunches(search: "") { launches in
            XCTAssertFalse(launches.isEmpty, "Expected filtered launches")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }

    func testSearchLaunches_WithKeyword_ReturnsFilteredLaunches() {
        let expectation = XCTestExpectation(description: "Search launches with keyword")
        
        mySpaceX.searchLaunches(search: "Falcon") { launches in
            XCTAssertFalse(launches.isEmpty, "Expected filtered launches")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSearchLaunches_WithNonMatchingKeyword_ReturnsEmptyArray() {
        let expectation = XCTestExpectation(description: "Search launches with non-matching keyword")
        
        mySpaceX.searchLaunches(search: "XYZ") { launches in
            XCTAssertTrue(launches.isEmpty, "Expected empty array")
            expectation.fulfill()
        }
        
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testSearchLaunches_Performance() throws {
        measure {
            let expectation = XCTestExpectation(description: "Search launches performance")
            
            mySpaceX.searchLaunches(search: "") { _ in
                // Do nothing, just measure the performance
                expectation.fulfill()
            }
            
            wait(for: [expectation], timeout: 5.0)
        }
    }

}
