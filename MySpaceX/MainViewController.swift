//
//  MainViewController.swift
//  MySpaceX
//
//  Created by Alisina on 20.05.23.
//

import UIKit
import SpaceXLogic

class MainViewController: UINavigationController {
    
    let cellID = "mainCellID"
    var spaceX = MainSpaceX()
    var launches: [Launch] = []
    
    let searchButton: UIButton = {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.backgroundColor = .blue.withAlphaComponent(0.6)
        btn.setTitle("Search", for: .normal)
        btn.addTarget(self, action: #selector(searchLaunches), for: .touchUpInside)
        btn.layer.cornerRadius = 5
        return btn
    }()
    
    let searchBox: UITextField = {
       let box = UITextField()
        box.translatesAutoresizingMaskIntoConstraints = false
        box.layer.cornerRadius = 3
        box.layer.borderWidth = 0.5
        box.layer.borderColor = UIColor.black.cgColor
        return box
    }()
    
    let launchesCollectionView : UICollectionView = {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.edgesForExtendedLayout = UIRectEdge.bottom
        launchesCollectionView.register(LaunchesCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        launchesCollectionView.backgroundColor = .white
        launchesCollectionView.delegate = self
        launchesCollectionView.dataSource = self
        
        self.view.backgroundColor = .white
        self.navigationBar.isTranslucent = false
        
        
        self.view.addSubview(launchesCollectionView)
        self.view.addSubview(searchButton)
        self.view.addSubview(searchBox)
        self.setupConstraints()
        
    }
    
    @objc private func searchLaunches(){
        DispatchQueue.main.async {
            let searchData = self.searchBox.text
            self.spaceX.searchLaunches(search: searchData ?? "") { launches in
                
                self.launches.removeAll()
                self.launches = launches
                
                DispatchQueue.main.async {
                    self.launchesCollectionView.reloadData()
                }
            }
        }
    }
    
    
    private func setupConstraints() {
        NSLayoutConstraint.activate([
            self.searchButton.topAnchor.constraint(equalTo: self.navigationBar.bottomAnchor, constant: 10),
            self.searchButton.widthAnchor.constraint(equalToConstant: 150),
            self.searchButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -10),
            self.searchButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            
            self.searchBox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 10),
            self.searchBox.trailingAnchor.constraint(equalTo: self.searchButton.leadingAnchor, constant: -10),
            self.searchBox.centerYAnchor.constraint(equalTo: self.searchButton.centerYAnchor, constant: 0),
            self.searchBox.heightAnchor.constraint(equalToConstant: 40),
            
            self.launchesCollectionView.topAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 10),
            self.launchesCollectionView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            self.launchesCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            self.launchesCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0)
            
        ])
    }


    
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let numberofLaunches = self.launches.count
        return numberofLaunches // How many cells to display
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let myCell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellID, for: indexPath) as? LaunchesCollectionViewCell else{
            return UICollectionViewCell()
        }
        
        let launch = launches[indexPath.item]
        
        myCell.configure(with: launch)
        
        return myCell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let launch = launches[indexPath.item]
        if let url = URL(string: launch.links.article ?? ""){
            UIApplication.shared.open(url)
        }else {
            print("No artical")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width - 20, height: 120)
    }
}
