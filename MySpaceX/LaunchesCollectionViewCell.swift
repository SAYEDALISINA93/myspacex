//
//  LaunchesCollectionViewCell.swift
//  MySpaceX
//
//  Created by Alisina on 20.05.23.
//

import UIKit
import SpaceXLogic

class LaunchesCollectionViewCell: UICollectionViewCell {
    private let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let flightNumberLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let dateLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let statusView: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private let logoImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        // Add subviews to the cell
        contentView.addSubview(nameLabel)
        contentView.addSubview(flightNumberLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(statusView)
        contentView.addSubview(logoImageView)
        
        // Add constraints for the subviews
        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            nameLabel.trailingAnchor.constraint(equalTo: logoImageView.leadingAnchor, constant: -8),
            
            flightNumberLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 4),
            flightNumberLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            flightNumberLabel.trailingAnchor.constraint(equalTo: logoImageView.leadingAnchor, constant: -8),
            
            dateLabel.topAnchor.constraint(equalTo: flightNumberLabel.bottomAnchor, constant: 4),
            dateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            dateLabel.trailingAnchor.constraint(equalTo: logoImageView.leadingAnchor, constant: -8),
            
            statusView.topAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 4),
            statusView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            statusView.trailingAnchor.constraint(equalTo: logoImageView.leadingAnchor, constant: -8),
            
            logoImageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            logoImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -8),
            logoImageView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -8),
            logoImageView.heightAnchor.constraint(equalToConstant: 100),
            logoImageView.widthAnchor.constraint(equalToConstant: 100)
        ])
        
        self.contentView.layer.cornerRadius = 8
    }
    
    func configure(with launch: Launch) {
        nameLabel.text = launch.name
        flightNumberLabel.text = "Flight Number: \(launch.flightNumber)"
        dateLabel.text = "Date: \(launch.formattedLaunchDate ?? "")"
        
        if launch.status ?? false {
            contentView.backgroundColor = .green.withAlphaComponent(0.15)
            statusView.text = "Mission Status: Succeed"
        } else {
            contentView.backgroundColor = .red.withAlphaComponent(0.15)
            statusView.text = "Mission Status: Failed"
        }
        
        //     Set the logo image using the appropriate URL or image data
        if let logoURL = URL(string: launch.links.patch.small ?? "") {
            // Load image from URL and set it to the logoImageView
            
            let model = MainSpaceX()
            
            model.loadImage(url: logoURL) { data in
                DispatchQueue.main.async {
                    self.logoImageView.image = UIImage(data: data)
                }
            }
        } else {
            // Set a default image if the logoURL is not available
            logoImageView.image = UIImage(named: "placeholder_image")
        }
    }}
