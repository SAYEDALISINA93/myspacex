# MySpaceX Project

The MySpaceX project is a simple iOS application that displays SpaceX launches and allows users to search for launches by a specific criteria. It utilizes the SpaceXLogic framework to retrieve and filter launch data.

## Features

- Display a collection of SpaceX launches in a scrollable list.
- Search launches by entering launch name.
- Launches are displayed in a collection view with each cell showing launch details.
- Tapping on a launch cell opens the associated article in a web browser.

## Installation

1. Clone the MySpaceX repository from GitLab.
2. Open the project in Xcode.
3. Build and run the project on a simulator or device running iOS.

## Dependencies

The MySpaceX project has the following dependencies:

- SpaceXLogic framework: Provides the logic for retrieving and filtering SpaceX launch data.

## Architecture

The project follows the Model-View-Controller (MVC) architectural pattern. Here's an overview of the key components:

- **MainViewController**: The main view controller that displays the list of SpaceX launches and handles user interactions.
- **LaunchesCollectionViewCell**: The collection view cell used to display individual launch details.
- **MainSpaceX**: A class that encapsulates the logic for retrieving and filtering launch data from the SpaceX API.
- **SpaceXLogic framework**: A separate framework that handles the backend logic for retrieving and filtering SpaceX launch data.

## Testing

The project includes XCTest unit tests and UI tests to ensure the correctness and functionality of the application. The tests cover various scenarios and edge cases.

To run the tests, select the "MySpaceX" scheme and choose "Test" from the "Product" menu or press `Cmd + U`.

## License

The MySpaceX project is open-source and released under the [MIT License](LICENSE).


